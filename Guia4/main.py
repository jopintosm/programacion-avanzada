import random
from estudiante import Estudiante
from asignatura import Asignatura
from diploma import Diploma


def main():

    #Creacion de objetos iniciales
    programacion = Asignatura("Programacion")
    fisica = Asignatura("Fisica")
    quimica = Asignatura("Quimica")


    #Creacion del entorno
    lista_alumnos = [Estudiante("Fabio"),
                     Estudiante("Arturo"),
                     Estudiante("Pepito")]

    
    lista_asignatura = [programacion,
                        fisica,
                        quimica]


    diplomas = []


    #Asignacion de asignaturas
    for i in lista_alumnos:
        for j in lista_asignatura:
            i.asignatura = j

    #DEBUG - Inscripcion de asignaturas detalladas
    for alumno in lista_alumnos:
        for asignatura in range(len(alumno.asignatura)):
            print(alumno.nombre," -> ",alumno.asignatura[asignatura].nombre)


    #Creacion de diplomas
    for i in lista_alumnos:
        for j in i.asignatura:
            diplomas.append(Diploma(i.nombre,j))


    #DEBUG - Creacion exitosa de los diplomas
    print("\n\nLos diplomas se crearon exitosamente\n\n")

    #DEBUG - Comprobacion de los diplomas
    for diploma in diplomas:
        print("Diploma de ", diploma.estudiante)


    #Repartición de los diplomas
    for diploma in diplomas:
        (random.choice(lista_alumnos)).diploma = diploma     


    #DEBUG - Comprobación del inventario de diplomas de los alumnos
    print("\n\n--- Momento INICIAL ---")
    for alumno in lista_alumnos:
        print("\nDiplomas de ",alumno.nombre,)
        for diploma in alumno._diploma:
            print(" -Diploma de ", diploma.estudiante)

        
    #Creacion de la situación
    while True:
        #Comprobación de que los diplomas sigan en desorden
        checker_individual = 0
        checker_colectivo = 0
        for alumno in lista_alumnos:
            for diploma in alumno.diploma:
                if alumno._nombre == diploma.estudiante:
                    checker_individual += 1

            if checker_individual == len(alumno.diploma):
                checker_colectivo += 1
        
        if checker_colectivo == len(lista_alumnos):
            break


        #Cambio de diploma
        for alumno in lista_alumnos:
            for diploma in alumno._diploma:
                if diploma.estudiante != alumno._nombre:
                    (random.choice(lista_alumnos)).diploma = diploma
                    alumno._diploma.remove(diploma)
                    


if __name__ == '__main__':
    main()