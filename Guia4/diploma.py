from asignatura import Asignatura


class Diploma():

    """docstring for Diploma."""

    def __init__(self, estudiante, asignatura):
        self._estudiante = estudiante
        self._asignatura = asignatura

     
    @property
    def estudiante(self):
        return self._estudiante
    
    @estudiante.setter
    def estudiante(self, nombre):
        if isinstance(nombre, str):
            self._estudiante = nombre
        else:
            print("El tipo de dato no corresponde")

            
    @property
    def asignatura(self):
        return self._asignatura
    
    @asignatura.setter
    def asignatura(self,asignatura):
        if isinstance(asignatura, Asignatura):
            self._asignatura = asignatura
        else:
            print("no pertenece a clase adecuada")