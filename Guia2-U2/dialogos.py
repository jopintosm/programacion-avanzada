# Renata Arcos - Josefa Pinto San Martin \ Guia 2 - Unidad 2
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogInfo(Gtk.MessageDialog):
    def __init__(self, cliente):
        # se crea la ventana y se aniade un boton
        super().__init__(title="INFORMACION", buttons=Gtk.ButtonsType.OK)
        # se formatea el texto del dialogo
        self.set_markup(f"Boleta de {cliente}")
        self.format_secondary_markup(f"Generada exitosamente")
        self.show_all()
        self.run()
        self.destroy()

class DialogError(Gtk.MessageDialog):
    def __init__(self):
        # se crea la ventana y se aniade un boton
        super().__init__(title="ERROR", buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.WARNING)
        # se formatea el texto del dialogo
        self.set_markup(f"No se generó la boleta, ya que")
        self.format_secondary_markup(f"faltan o no hay datos, intente nuevamente")
        self.show_all()
        self.run()
        self.destroy()