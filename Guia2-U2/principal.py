# Renata Arcos - Josefa Pinto San Martin \ Guia 2 - Unidad 2
from elegir import Seleccionar_Archivo
from crear import Crear_Boleta
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self):
        # creacion de ventana principal
        super().__init__(title="@koko.takoyaki") #spam

        self.box = Gtk.Box(spacing = 2)
        self.add(self.box)
        
        # se crea el boton para crear una boleta
        b_crear = Gtk.Button(label="CREAR BOLETA")
        b_crear.connect("clicked", self.crear)
        self.box.add(b_crear)

        # se crea el boton para ver la boleta hecha
        b_ver = Gtk.Button(label="VER BOLETA")
        b_ver.connect("clicked", self.ver)
        self.box.add(b_ver)

    def crear(self, btn=None):
        Crear_Boleta()
    def ver(self, btn=None):
        Seleccionar_Archivo()

if __name__ == "__main__":
    win = MainWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()