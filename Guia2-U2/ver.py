# Renata Arcos - Josefa Pinto San Martin \ Guia 2 - Unidad 2
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Ver_Boleta(Gtk.Window):
    def __init__(self, name_archivo):
        super().__init__(title="BOLETA") #spam

        # se crean los espacios
        self.box = Gtk.Box(spacing = 3)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.add(self.box)

        # generar tree y modelo
        tree = Gtk.TreeView()
        self.box.add(tree)
        
        # establecer modelo
        self.modelo = Gtk.ListStore(str, str, str)
        tree.set_model(model=self.modelo)

        # generar columnas
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRODUCTO",
                                cell,
                                text=0)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CANTIDAD",
                                cell,
                                text=1)
        tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRECIO",
                                cell,
                                text=2)
        tree.append_column(column)

        # crear "tabla" con producto, cantidad y precio
        # obtenidas de un archivo txt
        archivo = open(name_archivo)
        linea = 0 # inicializador
        aux = 0
        while linea != "":
            aux = aux + 1
            linea = archivo.readline()
            # luego de la linea 1 se comienza a extraer los
            # valores para añadirlos a la "tabla"
            if aux >= 2:
                substring = linea.strip().split(",")   
                producto = substring[0]
                try:
                    cantidad = substring[1]
                    precio = substring[2]

                    # reconocer la linea que tiene el comentario
                    if cantidad == "":
                        comentario = precio
                        comentario = comentario.replace("*","\n")
                    else:
                        self.modelo.append([producto,cantidad,precio])
                except IndexError:
                    total=linea
                    break

        # se asignan un label con el total de la boleta
        label = Gtk.Label()
        label.set_label(total)
        self.box.add(label)

        # se crean los espacios
        self.box1 = Gtk.Box(spacing = 2)
        self.box.add(self.box1)

        # se asignan un label con el total de la boleta
        label1 = Gtk.Label()
        label1.set_label("Comentario: ")
        self.box1.add(label1)

        # se carga el text view y su buffer (el comentario)
        self.textview = Gtk.TextView()
        # asi no se puede editar el texto
        self.textview.set_editable(False)
        self.text = self.textview.get_buffer()
        self.text.set_text(comentario)
        self.box1.pack_end(self.textview, True, True, 0)


        self.show_all()