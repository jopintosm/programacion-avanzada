# Renata Arcos - Josefa Pinto San Martin \ Guia 2 - Unidad 2
from dialogos import DialogInfo
from dialogos import DialogError
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Crear_Boleta(Gtk.Window):
    def __init__(self):
        # creacion de ventana para crear boleta
        super().__init__(title="CREACION DE BOLETA")

        #creacion de listas
        self.list = []
        self.precios = []

        # se divide la ventana en 6 verticalmente
        self.box = Gtk.Box(spacing=7)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.add(self.box)

        # en el primer compartimiento se añade un label
        label = Gtk.Label()
        label.set_label(f"ESCRIBA EL NOMBRE DEL CLIENTE, ELIJA LOS PRODUCTOS, LA CANTIDAD Y PRESIONE AGREGAR")
        self.box.add(label)
        
        # el segundo compartimiento se divide en 2
        self.box1 = Gtk.Box(spacing=2)
        self.box.add(self.box1)

        # se asignan identificador para cuadro de texto
        label1 = Gtk.Label()
        label1.set_label("INGRESE EL NOMBRE DEL CLIENTE: ")
        self.box1.add(label1)

        # se crean el cuadro de texto para el nombre del cliente
        self.entry = Gtk.Entry()
        self.entry.connect("activate", self.agregar)
        self.box1.add(self.entry)

        # el tercer compartimiento se divide en 2
        self.box2 = Gtk.Box(spacing=2)
        self.box.add(self.box2)

        # se asignan identificador para cuadro de texto
        label2 = Gtk.Label()
        label2.set_label("INGRESE UN COMENTARIO: ")
        self.box2.add(label2)
        
        # se carga el text view y su buffer(el texto que muestra)
        self.textview = Gtk.TextView()
        self.text = self.textview.get_buffer()

        # se setea el texto por default del textview
        # y se añade al tercer compartimiento
        self.review = "None"
        self.text.set_text(self.review)
        self.box2.pack_end(self.textview, True, True, 0)

        # el cuarto compartimiento se divide en 2
        self.box3 = Gtk.Box(spacing=2)
        self.box.add(self.box3)

        # se añade un combobox para elegir el producto
        self.combobox_producto = Gtk.ComboBoxText()
        self.box3.add(self.combobox_producto)

        # se añade un combobox para elegir la cantidad
        self.combobox_cantidad = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.combobox_cantidad.append_text(str(x))
        self.combobox_cantidad.set_active(0)
        self.box3.add(self.combobox_cantidad)

        # lista modelo
        self.lista = Gtk.ListStore(str)
        self.lista = ["Takoyakis pulpo", "Takoyakis camaron", "Takoyaki mix",
                      "Yakitori", "Okonomiyaki", "Gyozas", "Dango", "Bebida"]

        # Se carga modelo en combobox de productos
        for item in self.lista:
            self.combobox_producto.append_text(item)
        self.combobox_producto.set_active(0)
        
        # se añade un combobox para elegir el despacho
        self.combobox_despacho = Gtk.ComboBoxText()
        self.box.add(self.combobox_despacho)

        # lista modelo
        self.lista1 = Gtk.ListStore(str)
        self.lista1 = ["Consume en el local", "Retira en el local", "Delivery"]

        # Se carga modelo en combobox de productos
        for item in self.lista1:
            self.combobox_despacho.append_text(item)
        self.combobox_despacho.set_active(0)
        
        # el sexto compartimiento se divide en 2
        self.box4 = Gtk.Box(spacing=2)
        self.box.add(self.box4)

        # boton agregar
        btn_agregar = Gtk.Button(label="AGREGAR")
        btn_agregar.connect("clicked", self.agregar)
        self.box4.add(btn_agregar)

        # boton borrar
        btn_agregar = Gtk.Button(label="BORRAR TODO")
        btn_agregar.connect("clicked", self.borrar)
        self.box4.add(btn_agregar)


        # se agrega un boton para generar boleta en el sexto compartimiento
        btn_generar = Gtk.Button(label="GENERAR BOLETA")
        btn_generar.connect("clicked", self.gen_boleta)
        self.box.add(btn_generar)

        self.show_all()


    def agregar(self, btn=None):
        """Metodo para agregar el nombre del cliente, items, productos,
        cantidades y valores a las listas. Tambien se formatean las lineas de texto
        para posteriormente ser utilizadas para escribir el texto formato csv"""
        
        # obtener nombre(en mayus), producto, cantidad
        self.name = self.entry.get_text().upper()
        self.seleccion_producto = self.combobox_producto.get_active_text()
        self.seleccion_cantidad = self.combobox_cantidad.get_active_text()

        # obtener comentario del textview
        start = self.text.get_start_iter()
        end = self.text.get_end_iter()
        self.comentario = self.text.get_text(start, end, False)

        # setear linea fija para despues agregar el archivo
        cliente = f'Nombre del cliente: {self.name}'

        # para ingresar solo una vez el nombre del cliente
        if cliente in self.list:
            pass
        else:
            self.list.append(cliente)

        # diccionario con los precios de cada producto
        dic_precios = {"Takoyakis pulpo": 3500,
                    "Takoyakis camaron": 3500,
                    "Takoyaki mix": 3500,
                    "Yakitori": 4500,
                    "Okonomiyaki": 4500, 
                    "Gyozas": 3000, 
                    "Dango": 2500, 
                    "Bebida": 1200}

        # se multiplican las cantidades por el precio del producto
        valor = dic_precios[self.seleccion_producto]*int(self.seleccion_cantidad)

        # se agrega a la lista de precios
        self.precios.append(valor)

        # se setea la nueva linea con el producto, cantidad, precio
        nueva_linea = f"\n{self.seleccion_producto},{self.seleccion_cantidad},${dic_precios[self.seleccion_producto]}"
        
        # se agrega a la lista de lineas para despues escribir el archivo
        self.list.append(nueva_linea)

        # todo el comentario se escribe en solo una linea
        self.comentario = self.comentario.replace("\n","*")

        # se setea del comentario
        self.commit = f"\n,,{self.comentario}"
        

    def borrar(self, btn=None):
        self.list = []
        self.entry.set_text("")


    def gen_boleta(self, btn=None):
        # se usan excepciones para verificar si se ingreso nombre de cliente
        try:
            # en caso de haber usado el boton borrar
            if self.list != []:
                if self.name != "":
                    # se agrega a la lista de lineas para despues escribir el archivo
                    self.list.append(self.commit)
                    # calcular el total de la boleta
                    total = 0 #inicializador
                    for i in self.precios:
                        total = total + int(i)
                    self.list.append(f"\nEl total es ${total}")

                    # escribir el archivo a partir de la lista creada
                    # el nombre del archivo corresponde al nombre del cliente
                    # mas la extension.
                    with open(f"{self.name}.txt", "w") as file:
                        file.write("".join(self.list))
                    file.close()

                    # cerrar ventana
                    self.destroy()

                    # informar que la boleta se genero con exito
                    DialogInfo(self.name)
                else:
                    DialogError()
            else:
                DialogError()
                
        except AttributeError:
            DialogError()