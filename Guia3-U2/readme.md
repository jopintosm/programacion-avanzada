# Programacion Avanzada - Guia 3 Unidad 2


### Integrantes
* Renata Arcos Williams
* Josefa Pinto San Martin

---

El programa consiste en una plantilla para construir una boleta de un restaurant de comida
japonesa y tambien para poder visualizar las boletas generadas.

### Instruciones de uso de la aplicacion
1. Elija si quiere crear o ver una boleta.
2. Si elige la opcion de "CREAR BOLETA", debe llenar el formulario que aparecerá en
la nueva ventana, presione "AGREGAR" cada vez que ingrese un producto y finalmente presione
"GENERAR BOLETA" para que la boleta quede guardada como un archivo de texto.
3. Si elige la opcion de "VER BOLETA", aparecerá una ventana que le permitirá elegir un archivo,
donde deberá elegir la boleta que desea ver. Luego de que seleccione la boleta con el boton "Abrir",
se mostrará una ventana donde estará el detalle de los productos, el total a pagar, el tipo de despacho
y si se hizo algun comentario.

## Ventana principal
La clase MainWindow, es una clase heredada del tipo Gtk.Window; a esta ventana se le agrega un objeto
Gtk.HeaderBar, el cual reemplaza al que viene por defecto con la ventana. Este objeto posee un titulo,
un subtitulo y un boton identificado con una imagen; dicho boton, instancia una ventana tipo 
Gtk.AboutDialog la cual posee informacion sobre el programa y las creadoras de este. 
Esta ventana tambien tiene un Gtk.Box para dividir en 2 el espacio, donde posteriormente se agregan dos
botones tipo Gtk.Button, el primero, se activa haciendo click en él y deriva a un método que instancia la
ventana "Crear_Boleta". El segundo de igual manera se activa haciendo click en él y deriva a un método que
instancia la ventana "Seleccionar_Archivo". Es importante mencionar que al ser la ventana principal, si
esta se cierra, el programa termina de ejecutarse.

## Ventana Crear Boleta
Esta ventana es heredada de un widget tipo Gtk.Window; este widget posee un Gtk.Box dividido en 2
horizontalmente:

El lado izquierdo posee un Gtk.Box dividido en 6 espacios verticalmente, los cuales estan distribuidos
de la siguiente manera:

1. Un Gtk.Box dividido en 2 para colocar al lado izquierdo un label identificador y al lado derecho un
cuadro de texto tipo Gtk.Entry para que se pueda ingresar el nombre del cliente.
2. Un Gtk.Box dividido en 2 para colocar al lado izquierdo un label identificador y al lado derecho un
Gtk.TextView y su buffer, seteado con un texto inicial, el cual es editable.
3. Un Gtk.Box dividido en 3 para colocar al lado izquierdo un label identificador, al centro un objeto
Gtk.ComboBoxText el cual se llena con una lista de productos que son vendidos en el local, y al lado
derecho un Gtk.ComboBoxText el cual se llena con numeros (1-10) los cuales son usados para elegir la
cantidad de producto.
4. Un Gtk.Box dividido en 2 para colocar al lado izquierdo un label identificador y al lado derecho un
Gtk.ComboBoxText el cual se llena con una lista de tipos de despacho de los productos.
5. Un Gtk.Button llamado "AGREGAR", el cual instancia un metodo que se encarga de obtener los valores para
formar y ordenar una lista con strings, los cuales proximamente seran utilizados para escribir el archivo.
6. Finalmente hay un Gtk.Button llamado "GENERAR BOLETA", el cual al estar en su modo activo, lo que
depende de que si se ingresó un nombre o no; si no se ingresó un nombre para el cliente, el boton estará
desactivado y se instanciará una ventana Gtk.MessageDialog tipo Warning, la cual advertirá que se debe
ingresar un nombre para el cliente; este boton instancia un método que se encarga de obtener algunos
valores y escribir el archivo txt en base a la lista anteriormente generada; el nombre que recibirá el
archivo es el nombre del cliente + .txt.

Si la boleta es generada exitosamente, se instanciará una ventana informativa tipo Gtk.MessageDialog, la
cual tendrá el nombre del cliente e informará que no hubo errores en la creacion de la boleta.

El lado derecho posee un Gtk.ScrolledWindow, el cual permite hacer scroll en este espacio de la ventana,
con el fin de poder visualizar de buena manera todos los datos. Dentro de esta ventana existe un
Gtk.TreeView que ordena los productos, las cantidades y el precio en una "tabla", estos se van añadiendo
directamente al modelo del TreeView con el boton Añadir. 

## Ventana Seleccionar Archivo
Esta ventana es heredada de un widget tipo Gtk.FileChooserDialog; este widget sirve para elegir el archivo
que posteriormente se mostrará en el detalle de la boleta como tal. Se setea un filtro para que solo se
muestren archivos con extension .txt; la ventana posee dos botones "Cancelar" y "Abrir", en caso de
clickear el primero, se cerrará la ventana. En caso de clickear el segundo, se obtendrá la ruta del
archivo que se seleccionó y desde ahí se extrae el nombre del archivo seleccionado. Luego de seleccionar
un archivo de texto, la ventana se cierra y se instancia otro widget llamado "Ver_Boleta", el cual tiene
como argumento el nombre del archivo.

## Ventana Ver Boleta
Esta ventana es heredada de un widget tipo Gtk.Window; este widget posee un Gtk.Box dividido en 4
espacios verticalmente, los cuales estan distribuidos de la siguiente manera:

1. Un Gtk.ScrolledWindow, el cual permite hacer scroll en este espacio de la ventana, con el fin de poder
visualizar de buena manera todos los datos. Dentro de esta ventana se utiliza un Gtk.TreeView para
observar de manera ordenada los productos, cantidades y precios, esto se llena utilizando directamente el
archivo de texto seleccionado anteriormente.
2. Un Gtk.label, que se utiliza para mostrar el valor total a pagar.
3. Un Gtk.label, que se utiliza para mostrar el tipo de despacho escogido.
4. Un Gtk.Box dividido en 2 para colocar al lado izquierdo un label identificador y al lado derecho un Gtk
TextView y su buffer, seteado con el comentario de la boleta, el cual es un texto multilinea no editable.