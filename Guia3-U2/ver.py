# Renata Arcos - Josefa Pinto San Martin \ Guia 3 - Unidad 2
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Ver_Boleta(Gtk.Window):
    def __init__(self, name_archivo):
        
        # para saber el nombre del cliente
        cliente = name_archivo.split(".")
        cliente = cliente[0]
        
        # creacion de ventana para visualizar boleta
        super().__init__(title=f"BOLETA DE {cliente}") #spam

        # caracteristicas de la ventana
        self.set_default_size(285, 350)
        self.set_resizable(False)

        # se crean los espacios
        self.box = Gtk.Box(spacing = 4)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.add(self.box)

        # añadir scroll al treeview
        self.scroll = Gtk.ScrolledWindow()
        self.box.pack_start(self.scroll, True, True, 0)

        # generar treeview y modelo
        self.treeview = Gtk.TreeView()
        self.scroll.add(self.treeview)

        # establecer modelo
        self.modelo = Gtk.ListStore(str, str, str)
        self.treeview.set_model(model=self.modelo)

        # generar columnas
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRODUCTO",
                                cell,
                                text=0)
        self.treeview.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CANTIDAD",
                                cell,
                                text=1)
        self.treeview.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRECIO",
                                cell,
                                text=2)
        self.treeview.append_column(column)

        # crear "tabla" con producto, cantidad y precio
        # obtenidas de un archivo txt
        archivo = open(name_archivo)
        linea = 0 # inicializador
        aux = 0
        while linea != "":
            aux = aux + 1
            linea = archivo.readline()
            # luego de la linea 1 se comienza a extraer los
            # valores para añadirlos al treeview.
            if aux >= 2:
                substring = linea.strip().split(",")   
                producto = substring[0]
                try:
                    cantidad = substring[1]
                    precio = substring[2]
                    # reconocer la linea que tiene el tipo de despacho
                    if cantidad == "*":
                        despacho = precio
                    # reconocer la linea que tiene el comentario
                    elif cantidad == "":
                        comentario = precio
                        comentario = comentario.replace("*","\n")
                    else:
                        self.modelo.append([producto,cantidad,precio])
                except IndexError:
                    # utilizando try and except para reconocer la linea final
                    total=linea
                    break

        # se asignan un label con el total de la boleta
        label = Gtk.Label()
        label.set_label(total)
        self.box.add(label)

        # se asignan un label con el despacho de la boleta
        label_despacho = Gtk.Label()
        label_despacho.set_label(f"Despacho: {despacho}")
        self.box.add(label_despacho)

        # se divide en 2 el tercer espacio
        self.box1 = Gtk.Box(spacing = 2)
        self.box.add(self.box1)

        # se asigna un label identificador
        label1 = Gtk.Label()
        label1.set_label("Comentarios: ")
        self.box1.add(label1)

        # se carga el text view y su buffer (el comentario)
        self.textview = Gtk.TextView()
        self.text = self.textview.get_buffer()

        # de esta manera se vuelve un texto no editable
        self.textview.set_editable(False)
        self.textview.set_cursor_visible(False)
        
        # se setea el texto del text view
        self.text.set_text(comentario)

        # se añade el textview a la ventana
        self.box1.pack_end(self.textview, True, True, 0)

        self.show_all()
