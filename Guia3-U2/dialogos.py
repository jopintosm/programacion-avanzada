# Renata Arcos - Josefa Pinto San Martin \ Guia 3 - Unidad 2
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogInfo(Gtk.MessageDialog):
    def __init__(self, cliente):
        # se crea la ventana y se aniade un boton
        super().__init__(title="INFORMACION", buttons=Gtk.ButtonsType.OK)
        # se formatea el texto del dialogo
        self.set_markup(f"Boleta de {cliente}")
        self.format_secondary_markup(f"Generada exitosamente")
        self.show_all()
        self.run()
        self.destroy()

class DialogErrorName(Gtk.MessageDialog):
    def __init__(self):
        # se crea la ventana y se aniade un boton
        super().__init__(title="ERROR", buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.WARNING)
        # se formatea el texto del dialogo
        self.set_markup(f"Debe ingresar el nombre")
        self.format_secondary_markup(f"del cliente")
        self.show_all()
        self.run()
        self.destroy()

class Ventana_About(Gtk.AboutDialog):
    def __init__(self):
        # creacion de ventana about
        super().__init__(title="About us")
        self.set_default_size(600, 300)
        # setear los diferentes textos del about us
        self.set_authors(["RENATA ARCOS WILLIAMS", "JOSEFA PINTO SAN MARTIN"])
        self.set_program_name("Creador y visualizador de boletas @KOKO TAKOYAKI")
        self.set_version("Versión 1.1")
        self.set_logo_icon_name("applications-graphics")
        self.set_comments("Profesor Fabio Durán Verdugo")
        self.set_copyright("Ingeniería Civil en Bioinformática - Programacion avanzada")
        self.set_website("https://instagram.com/koko.takoyaki?igshid=NWRhNmQxMjQ=")
        self.show_all()