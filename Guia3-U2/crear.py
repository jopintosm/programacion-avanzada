# Renata Arcos - Josefa Pinto San Martin \ Guia 3 - Unidad 2
from dialogos import DialogInfo
from dialogos import DialogErrorName
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Crear_Boleta(Gtk.Window):
    def __init__(self):
        # creacion de ventana para crear boleta
        super().__init__(title="CREACION DE BOLETA")

        # caracteristicas de la ventana
        self.set_default_size(700, 150)
        self.set_resizable(False)

        # creacion de listas
        self.list_pedido = []
        self.precios = []

        # se divide la ventana en 2 horizontalmente
        self.box0 = Gtk.Box(spacing=2)
        self.add(self.box0)

        # se divide la ventana izquierda en 6 verticalmente
        self.box = Gtk.Box(spacing=6)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.box0.add(self.box)
            
        # añadir scroll al treeview
        self.scroll = Gtk.ScrolledWindow()
        self.box0.pack_end(self.scroll, True, True, 0)

        # en el lado derecho se generar tree y modelo
        self.tree = Gtk.TreeView()
        self.scroll.add(self.tree)

        # establecer modelo
        self.modelo = Gtk.ListStore(str, str, str)
        self.tree.set_model(model=self.modelo)

        # generar columnas
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRODUCTO",
                                cell,
                                text=0)
        self.tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("CANTIDAD",
                                cell,
                                text=1)
        self.tree.append_column(column)

        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("PRECIO",
                                cell,
                                text=2)
        self.tree.append_column(column)

        # el primer compartimiento derecho se divide en 2
        self.box1 = Gtk.Box(spacing=2)
        self.box.add(self.box1)

        # se asignan identificador para cuadro de texto
        label1 = Gtk.Label()
        label1.set_label("Ingrese el nombre del cliente: ")
        self.box1.add(label1)

        # se crean el cuadro de texto para el nombre del cliente
        self.entry = Gtk.Entry()
        self.entry.connect("activate", self.agregar)
        self.box1.add(self.entry)

        # el segundo compartimiento se divide en 2
        self.box2 = Gtk.Box(spacing=2)
        self.box.add(self.box2)

        # se asignan identificador para cuadro de texto
        label2 = Gtk.Label()
        label2.set_label("Ingrese un comentario: ")
        self.box2.add(label2)
        
        # se carga el text view y su buffer(el texto que muestra)
        self.textview = Gtk.TextView()
        self.text = self.textview.get_buffer()

        # se setea el texto por default del textview
        self.review = "None"
        self.text.set_text(self.review)
        self.box2.pack_end(self.textview, True, True, 0)

        # el tercer compartimiento se divide en 3
        self.box3 = Gtk.Box(spacing=3)
        self.box.add(self.box3)

        # se asigna un label identificador
        label_p = Gtk.Label()
        label_p.set_label("Elija producto y cantidad: ")
        self.box3.pack_start(label_p, True, True, 0)

        # se añade un combobox para elegir el producto
        self.combobox_producto = Gtk.ComboBoxText()
        self.box3.add(self.combobox_producto)

        # se añade un combobox para elegir la cantidad
        self.combobox_cantidad = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.combobox_cantidad.append_text(str(x))
        self.combobox_cantidad.set_active(0)
        self.box3.add(self.combobox_cantidad)

        # lista modelo
        self.lista = Gtk.ListStore(str)
        self.lista = ["Takoyakis pulpo", "Takoyakis camaron", "Takoyaki mix",
                      "Yakitori", "Okonomiyaki", "Gyozas", "Dango", "Bebida"]

        # Se carga modelo en combobox de productos
        for item in self.lista:
            self.combobox_producto.append_text(item)
        self.combobox_producto.set_active(0)

        # el cuarto compartimiento se divide en 2
        self.box4 = Gtk.Box(spacing=2)
        self.box.add(self.box4)
        
        # se asigna un label identificador
        label_d = Gtk.Label()
        label_d.set_label("Elija el despacho de sus productos: ")
        self.box4.add(label_d)

        # se añade un combobox para elegir el despacho
        self.combobox_despacho = Gtk.ComboBoxText()
        self.box4.add(self.combobox_despacho)

        # lista modelo
        self.lista1 = Gtk.ListStore(str)
        self.lista1 = ["Consumo en el local", "Retiro en el local", "Delivery"]

        # Se carga modelo en combobox de productos
        for item in self.lista1:
            self.combobox_despacho.append_text(item)
        self.combobox_despacho.set_active(0)

        # boton agregar en el quinto compartimiento
        btn_agregar = Gtk.Button(label="AGREGAR")
        btn_agregar.connect("clicked", self.agregar)
        self.box.pack_start(btn_agregar, True, True, 0)

        # se agrega un boton para generar boleta en el sexto compartimiento
        self.btn_generar = Gtk.Button(label="GENERAR BOLETA")
        self.btn_generar.connect("clicked", self.gen_boleta)
        self.box.add(self.btn_generar)
        self.btn_generar.set_sensitive(False)

        self.show_all()


    def agregar(self, btn=None):

        """Metodo para obtener y agregar el nombre del cliente, un comentario, productos,
        cantidades, valores y despacho a las listas. Tambien se formatean las lineas de texto
        para posteriormente ser utilizadas para escribir el texto formato txt"""
        
        # obtener nombre (en mayuscula)
        self.name = self.entry.get_text().upper()

        # si no se ha ingresado un nombre, se informa 
        if self.name == "":
            DialogErrorName()
        # si ya existe un nombre, se setean y se agregan los demas items.
        else:
            # se activa el boton generar boleta
            self.btn_generar.set_sensitive(True)

            # se obtienen los valores
            self.seleccion_producto = self.combobox_producto.get_active_text()
            self.seleccion_cantidad = self.combobox_cantidad.get_active_text()
            self.seleccion_despacho = self.combobox_despacho.get_active_text()

            # obtener comentario del textview
            start = self.text.get_start_iter()
            end = self.text.get_end_iter()
            self.comentario = self.text.get_text(start, end, False)

            # setear linea fija para despues agregar el archivo
            self.cliente = f'Nombre del cliente: {self.name}'

            # para ingresar solo una vez el nombre del cliente
            if self.cliente in self.list_pedido:
                pass
            else:
                self.list_pedido.append(self.cliente)

            # diccionario con los precios de cada producto
            dic_precios = {"Takoyakis pulpo": 3500,
                        "Takoyakis camaron": 3500,
                        "Takoyaki mix": 3500,
                        "Yakitori": 4500,
                        "Okonomiyaki": 4500, 
                        "Gyozas": 3000, 
                        "Dango": 2500, 
                        "Bebida": 1200}

            # se multiplican las cantidades por el precio del producto
            valor = dic_precios[self.seleccion_producto]*int(self.seleccion_cantidad)

            # se agrega a la lista de precios
            self.precios.append(valor)

            # se setea la nueva linea con el producto, cantidad, precio
            self.nueva_linea = f"\n{self.seleccion_producto},{self.seleccion_cantidad},${dic_precios[self.seleccion_producto]}"

            # se agrega a la lista de lineas para despues escribir el archivo
            self.list_pedido.append(self.nueva_linea)

            # todo el comentario se escribe en solo una linea
            self.comentario = self.comentario.replace("\n","*")

            # se setea linea del comentario
            self.commit = f"\n,,{self.comentario}"

            # se setea linea del despacho
            self.despacho = f"\n,*,{self.seleccion_despacho}"

            # se agrega la linea al tree view
            self.modelo.append([self.seleccion_producto,self.seleccion_cantidad,
                                f"${dic_precios[self.seleccion_producto]}"])

    def gen_boleta(self, btn=None):

        """Metodo para agregar lineas faltantes y escribir el archivo txt"""

        # se agrega a la lista de lineas para despues escribir el archivo
        self.list_pedido.append(self.commit)

        # se agrega a la lista de lineas para despues escribir el archivo
        self.list_pedido.append(self.despacho)
        
        # calcular el total de la boleta
        total = 0 #inicializador
        for i in self.precios:
            total = total + int(i)
        self.list_pedido.append(f"\nEl total a pagar es ${total}")

        # escribir el archivo a partir de la lista creada
        # el nombre del archivo corresponde al nombre del cliente
        # mas la extension.
        with open(f"{self.name}.txt", "w") as file:
            file.write("".join(self.list_pedido))
        file.close()

        # cerrar ventana
        self.destroy()

        # informar que la boleta se genero con exito
        DialogInfo(self.name)