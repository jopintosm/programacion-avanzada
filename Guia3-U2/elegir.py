# Renata Arcos - Josefa Pinto San Martin \ Guia 3 - Unidad 2
from ver import Ver_Boleta
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class Seleccionar_Archivo(Gtk.FileChooserDialog):
    def __init__(self):
        super().__init__(title="SELECCIONAR ARCHIVO")
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, 
                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK)

        # crear filtro de archivos txt
        filter_txt = Gtk.FileFilter()
        filter_txt.add_pattern("*.txt")
        filter_txt.set_name("Archivos TXT")
        self.add_filter(filter_txt)

        # dependiendo del boton que se elija
        # se ejecuta una accion
        response = self.run()
        # si presiona abrir se obtiene el nombre del archivo
        if response == Gtk.ResponseType.OK:
            path = self.get_filename()
            path = path.split("/")
            self.name_archivo = path[-1]
            self.destroy()

        # si presiona cancelar se cierra la ventana
        elif response == Gtk.ResponseType.CANCEL:
            self.destroy()

        # se instancia esta clase para ver los datos de forma ordenada
        Ver_Boleta(self.name_archivo)