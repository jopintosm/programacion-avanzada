# Renata Arcos - Josefa Pinto San Martin \ Guia 3 - Unidad 2
from dialogos import Ventana_About
from elegir import Seleccionar_Archivo
from crear import Crear_Boleta
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self):
        # creacion de ventana principal
        super().__init__() 
        self.set_default_size(600, 80)
        self.set_resizable(False)

        # creacion de imagen
        img_info = Gtk.Image().new_from_file("/home/josefap/Escritorio/Programacion Avanzada/Guia3-U2/i.png")

        # se crea el boton para ver el about us
        self.b_about = Gtk.Button()

        # se agrega imagen al boton
        self.b_about.set_image(img_info)
        self.b_about.connect("clicked", self.show_about)

        # se setea la header bar con sus caracteristicas
        self.headerbar = Gtk.HeaderBar()
        self.headerbar.set_title("Creador y visualizador de boletas")
        self.headerbar.set_subtitle("@koko.takoyaki")#spam
        self.headerbar.set_show_close_button(True)
        self.headerbar.pack_start(self.b_about)

        # la headerbar por defecto es reemplaxada por la nueva headerbar
        self.set_titlebar(self.headerbar)

        # se divide en 2 horizontalmente la ventana
        self.box = Gtk.Box(spacing = 2)
        self.add(self.box)
        
        # se crea el boton para crear una boleta
        b_crear = Gtk.Button(label="CREAR BOLETA")
        b_crear.connect("clicked", self.show_crear)
        self.box.pack_start(b_crear, True, True, 0)

        # se crea el boton para ver la boleta hecha
        b_ver = Gtk.Button(label="VER BOLETA")
        b_ver.connect("clicked", self.show_ver)
        self.box.pack_end(b_ver, True, True, 1)

    def show_crear(self, btn=None):
        Crear_Boleta()

    def show_ver(self, btn=None):
        Seleccionar_Archivo()

    def show_about(self, btn=None):
        Ventana_About()

if __name__ == "__main__":
    win = MainWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()