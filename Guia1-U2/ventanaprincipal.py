# Guia 1 - unidad 2 / Renata Arcos - Josefa Pinto

from dialogoA import DialogInfo
from dialogoR import DialogReset
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class MainWindow(Gtk.Window):
    def __init__(self):
        # creacion de ventana principal
        super().__init__(title="Ventana principal")

        # crear los espacios para los widgets
        self.box = Gtk.Box(spacing=2)
        self.add(self.box)
        self.box1 = Gtk.Box(orientation=1, spacing=3)
        self.box.add(self.box1)
        self.box2 = Gtk.Box(orientation=1, spacing=3)
        self.box.add(self.box2)

        # se asignan identificadores a cuadros de texto
        label1 = Gtk.Label()
        label1.set_label("TEXTO 1: ")
        self.box1.add(label1)

        label2 = Gtk.Label()
        label2.set_label("TEXTO 2: ")
        self.box2.add(label2)

        # se crean los cuadros de texto
        self.entry1 = Gtk.Entry()
        self.entry1.connect("activate", self.suma)
        self.box1.add(self.entry1)

        self.entry2 = Gtk.Entry()
        self.entry2.connect("activate", self.suma)
        self.box2.add(self.entry2)

        # se crea el boton para hacer la suma
        button1 = Gtk.Button(label="ACEPTAR")
        button1.connect("clicked", self.suma)
        self.box1.add(button1)

        # se crea el boton para resetear los cuadros de texto
        button2 = Gtk.Button(label="RESET")
        button2.connect("clicked", self.clear)
        self.box2.add(button2)

    def clear(self, btn=None):
        """Se encarga de limpiar las entradas de texto"""
        dialogoReset = DialogReset()
        # dependiendo del boton seleccionado en la ventana
        # de confirmacion, se borra el texto o no.
        respuesta = dialogoReset.run()
        if respuesta == Gtk.ResponseType.YES:
            self.entry1.set_text("")
            self.entry2.set_text("")
        dialogoReset.destroy()

    def suma(self, btn=None):
        """Esta función determina si la entrada son numeros o textos,
        y dependiendo de esto, realiza la suma que posteriormente se
        muestra el resultado en el dialogo informativo"""

        self.texto = self.entry1.get_text()
        self.texto1 = self.entry2.get_text()
        aux = self.texto.isnumeric()
        if aux == True:
            # se usa el valor del numero para la suma
            self.valor = int(self.texto)
        else:
            # se usa el largo del texto para la suma
            self.valor = len(self.texto)

        aux1 = self.texto1.isnumeric()
        if aux1 == True:
            # se usa el valor del numero para la suma
            self.valor1 = int(self.texto1)
        else:
            # se usa el largo del texto para la suma
            self.valor1 = len(self.texto1)

        self.total = int(self.valor + self.valor1)

        # se abre un a ventana informativa con los valores ingresados
        # y el resultado de la operacion
        DialogInfo(self.texto, self.texto1, self.total)


if __name__ == "__main__":
    win = MainWindow()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()
