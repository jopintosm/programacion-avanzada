# Guia 1 - unidad 2 / Renata Arcos - Josefa Pinto

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DialogInfo(Gtk.MessageDialog):
    def __init__(self, valor, valor1,total):
        # se crea la ventana y se aniade un boton
        super().__init__(title="Datos ingresados", buttons=Gtk.ButtonsType.CANCEL)
        # se formatean los textos del dialogo
        self.set_markup(f"Primer elemento: {valor} \n Segundo elemento: {valor1}")
        self.format_secondary_markup(f"Resultado: {total}")
        self.show_all()
        self.run()
        self.destroy()
