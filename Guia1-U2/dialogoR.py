# Guia 1 - unidad 2 / Renata Arcos - Josefa Pinto

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogReset(Gtk.MessageDialog):
    def __init__(self):
        # se crea la ventana tipo warning y se aniaden dos botones
        super().__init__(title="Datos ingresados", buttons=Gtk.ButtonsType.YES_NO,
                         message_type=Gtk.MessageType.WARNING)
        self.set_markup(f"Esta seguro que desea resetear los cuadros de texto?")
        self.show_all()